FROM alpine:3.8

LABEL Maintainer="Zane M. Kolnik <zanematthew@gmail.com>" \
      Description="Lightweight container with Nginx 1.14 & PHP-FPM 7.2 based on Alpine Linux. A variation of; https://github.com/TrafeX/docker-php-nginx/"

# Install packages
RUN apk add \
    nginx \
    php7 \
    php7-fpm \
    php7-curl \
    php7-zlib \
    php7-phar \
    php7-intl \
    php7-dom \
    php7-xmlreader \
    php7-xmlwriter \
    php7-simplexml \
    php7-gd \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mbstring \
    php7-tokenizer \
    php7-xml \
    php7-ctype \
    php7-json \
    supervisor \
    curl \
    git \
    bash

# Install
RUN apk add \
    nodejs \
    nodejs-npm

# Configure NGINX
COPY config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/zzz_custom.conf
COPY config/php.ini /etc/php7/conf.d/zzz_custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Install composer
RUN curl -s https://getcomposer.org/installer | php -- \
    --install-dir=/usr/local/bin/ \
    --filename=composer \
    --version=1.7.2
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"

# Add application
RUN mkdir -p /var/www/html
WORKDIR /var/www/html
COPY src/ /var/www/html/

# Open ports
EXPOSE 80 443

# Set ENV variables
# @TODO automate this.
ENV NODEJS_VERSION=8.11.4
ENV NODEJS_NPM_VERSION=5.6.0
ENV PHP_VERSION=7.2.10
ENV COMPOSER_VERSION=1.7.2

# Start php-fpm & NGINX
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
